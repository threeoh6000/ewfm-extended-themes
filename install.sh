#!/bin/bash
echo "Erasing .git from ewfm-extended-themes."
echo "Press y on any prompts."
rm -r ewfm-extended-themes/.git

echo "Shifting LICENSE and README around."
mv ewfm-extended-themes/LICENSE ewfm-extended-themes/EXTENDED-LICENSE
mv ewfm-extended-themes/README ewfm-extended-themes/EXTENDED-README

echo "Moving any theme packs to root."

echo "Copying all files into themes folder."
cp -r ewfm-extended-themes/* themes/*

echo "Cleaning up."
rm -r ewfm-extended-themes

echo "Complete. Feel free to delete install.sh"
